variable "prefix" {
  default = "mnjsa"
}

variable "project" {
  default = "my-nodejs-api"
}

variable "contact" {
  default = "lisaula"
}

variable "aws_region" {
  description = "The AWS region resources are created"
  default     = "us-east-1"
}

variable "ecs_task_execution_role_name" {
  description = "Ecs task execution role name"
  default     = "EcsLabTaskExecutionRole"
}

variable "az_count" {
  description = "Number of Azs to cover in a given region"
  default     = "2"
}

variable "app_image" {
  description = "Docker image to run in the ECS Cluster"
  default     = "registry.gitlab.com/upflex1/my-nodejs-api:latest"
}

variable "app_port" {
  description = "Port exposed by docker image to redirect traffict to"
  default     = 1337
}

variable "app_count" {
  description = "Number docker containers to run"
  default     = 1
}

variable "health_check_path" {
  default = "/api/"
}

variable "fargate_cpu" {
  description = "Fargate instance CPU units to provision (1 vCPU = 1024 Cpu units)"
  default     = "256"
}

variable "fargate_memory" {
  description = "Fargate instance memory to provision (in MiB)"
  default     = "512"
}

variable "domain" {
  default = "tiendaandromeda.com"
}

variable "alias" {
  description = "Endpoints to the cloud front resorce"
  default     = "my-app"
}





