# alb.tf

data "aws_acm_certificate" "web_app" {
  domain   = var.domain
  statuses = ["ISSUED"]
}


resource "aws_alb" "main" {
  name            = "${local.prefix}-load-balancer"
  subnets         = aws_subnet.public.*.id
  security_groups = [aws_security_group.lb.id]

}

resource "aws_alb_target_group" "app" {
  name        = "${local.prefix}-target-group"
  port        = 80
  protocol    = "HTTP"
  vpc_id      = aws_vpc.main.id
  target_type = "ip"

  health_check {
    healthy_threshold   = "3"
    interval            = "30"
    protocol            = "HTTP"
    matcher             = "200"
    timeout             = "3"
    path                = var.health_check_path
    unhealthy_threshold = "2"
  }
  tags = merge(
    local.common_tags,
    tomap({ "Name" = "${local.prefix}-alb-tg" })
  )
}

resource "aws_alb_listener" "web_app_listener" {
  load_balancer_arn = aws_alb.main.id
  port              = "443"
  protocol          = "HTTPS"
  ssl_policy        = "ELBSecurityPolicy-2016-08"
  certificate_arn   = data.aws_acm_certificate.web_app.arn

  default_action {
    type             = "forward"
    target_group_arn = aws_alb_target_group.app.id
  }
}

# Redirect all traffic from the ALB to the target group
resource "aws_alb_listener" "web_app_listener_redirect_http_to_https" {
  load_balancer_arn = aws_alb.main.id
  # port              = var.app_port
  port     = "80"
  protocol = "HTTP"

  default_action {
    target_group_arn = aws_alb_target_group.app.id
    type             = "redirect"
    redirect {
      port        = 443
      protocol    = "HTTPS"
      status_code = "HTTP_301"
    }
  }

}
