# ecs.tf

resource "aws_ecs_cluster" "main" {
  name = "${local.prefix}-cluster"
  tags = merge(
    local.common_tags,
    tomap({ "Name" = "${local.prefix}-cluster" })
  )

}

data "template_file" "web-app" {
  template = file("./template/ecs/web_app.json.tpl")

  vars = {
    app_image      = var.app_image //image from gitlab
    app_port       = var.app_port
    fargate_cpu    = var.fargate_cpu
    fargate_memory = var.fargate_memory
    log_group_name = aws_cloudwatch_log_group.web_app_log_group.name
    aws_region     = var.aws_region
  }

}


resource "aws_ecs_task_definition" "api" {
  family                   = "${local.prefix}-api"
  execution_role_arn       = aws_iam_role.ecs_task_execution_role.arn
  network_mode             = "awsvpc"
  requires_compatibilities = ["FARGATE"]
  cpu                      = var.fargate_cpu
  memory                   = var.fargate_memory
  container_definitions    = data.template_file.web-app.rendered
  tags                     = local.common_tags

  //depends_on = [aws_cloudwatch_log_group.web_app_log_group]
}

resource "aws_ecs_service" "api" {
  name            = "${local.prefix}-api"
  cluster         = aws_ecs_cluster.main.id
  task_definition = aws_ecs_task_definition.api.family
  desired_count   = var.app_count
  launch_type     = "FARGATE"

  tags = local.common_tags

  network_configuration {
    security_groups  = [aws_security_group.ecs_tasks.id]
    subnets          = aws_subnet.private.*.id
    assign_public_ip = true
  }

  load_balancer {
    target_group_arn = aws_alb_target_group.app.id
    container_name   = "api"
    container_port   = var.app_port
  }


  depends_on = [aws_alb_listener.web_app_listener, aws_iam_role_policy_attachment.ecs_task_execution_role]
}

