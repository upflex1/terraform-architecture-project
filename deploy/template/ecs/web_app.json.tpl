[
  {
    "name": "api",
    "image": "${app_image}",
    "repositoryCredentials": {
            "credentialsParameter":"arn:aws:secretsmanager:us-east-1:429213281618:secret:gitlab_lci-wYI4a6"
        },
    "cpu": ${fargate_cpu},
    "memory": ${fargate_memory},
    "networkMode": "awsvpc",
    "logConfiguration": {
        "logDriver": "awslogs",
        "options": {
          "awslogs-group": "${log_group_name}",
          "awslogs-region": "${aws_region}",
          "awslogs-stream-prefix": "ecs"
        }
    },
    "portMappings": [
      {
        "containerPort": ${app_port},
        "hostPort": ${app_port}
      }
    ]
  }
]
