# Terraform-architecture-project
This project is a dependency of [my-nodejs-api](https://gitlab.com/upflex1/my-nodejs-api). This project contains all the terraform files that create the environment in AWS. In other words, it contains all the architecture that my-nodejs-api need to exist in AWS.

Nevertheless, this project does not have any execution purpose because is intended to execute from my-nodejs-api because of its CI/CD integration in Gitlab.  

***
## Authors and acknowledgment
Luis Carlos Isaula

